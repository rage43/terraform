#VPC_MASTER
resource "aws_vpc" "aaelogne_master_vpc" { # Creating VPC here
  cidr_block       = var.master_vpc_cidr   # Defining the CIDR block use 10.0.0.0/16
  instance_tenancy = "default"
  tags             = { Name = "aaelogne_main_vpc_master" }
}





#Create Internet Gateway and attach it to VPC
resource "aws_internet_gateway" "aaelogne_igw" { # Creating Internet Gateway
  vpc_id = aws_vpc.aaelogne_master_vpc.id        # vpc_id will be generated after we create VPC
  tags   = { Name = "aaelogne_igw" }
}


#Create a proxy private Subnets.
resource "aws_subnet" "aaelogne_privateSubnets_1" { # Creating Private Subnets
  vpc_id     = aws_vpc.aaelogne_master_vpc.id
  cidr_block = var.private_subnets_1 # CIDR block of public subnets
  tags       = { Name = "aaelogne_private_subnets_proxy" }
}


#Create admin private Subnets.
resource "aws_subnet" "aaelogne_privateSubnets_2" { # Creating Private Subnets
  vpc_id     = aws_vpc.aaelogne_master_vpc.id
  cidr_block = var.private_subnets_2 # CIDR block of public subnets
  tags       = { Name = "aaelogne_private_subnets_admin" }
}


#Route table for private_subnet Subnet's
resource "aws_route_table" "aaelogne_route_igw" { # Creating RT for Private Subnet
  vpc_id = aws_vpc.aaelogne_master_vpc.id
  tags   = { Name = "aaelogne_Private_route" }
  route {
    cidr_block = "0.0.0.0/0" # Traffic from Public Subnet reaches Internet via Internet Gateway
    gateway_id = aws_internet_gateway.aaelogne_igw.id
  }

  route {
    cidr_block                = "192.168.16.0/28" # Traffic from other VPC 
    vpc_peering_connection_id = aws_vpc_peering_connection.client_djamel.id
  }

  route {
    cidr_block                = "192.168.0.0/28" # Traffic from other VPC
    vpc_peering_connection_id = aws_vpc_peering_connection.client_pascal.id
  }

#route {
 #   cidr_block = "0.0.0.0/0"
  #  network_interface_id = aws_network_interface.interface_proxy.id
  #}

  depends_on = [aws_vpc_peering_connection.client_djamel, aws_vpc_peering_connection.client_pascal]

}

#Route table Association proxy
resource "aws_route_table_association" "aaelogne_PublicRTassociation_proxy" {
  subnet_id      = aws_subnet.aaelogne_privateSubnets_1.id
  route_table_id = aws_route_table.aaelogne_route_igw.id
}


#Route table Association admin
resource "aws_route_table_association" "aaelogne_PublicRTassociation_admin" {
  subnet_id      = aws_subnet.aaelogne_privateSubnets_2.id
  route_table_id = aws_route_table.aaelogne_route_igw.id
}
