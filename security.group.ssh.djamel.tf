resource "aws_security_group" "allow_ssh_djamel" {
  name        = "allow_ssh_djamel"
  description = "Allow ssh inbound traffic"
  vpc_id      = aws_vpc.client_djamel_vpc.id

  ingress {
    description = "SSH from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.2.0/24"]
  }


ingress {
    description = "PING ECHO 8"
    protocol    = "icmp"
    from_port   = 8
    to_port     = 0
    cidr_blocks = ["10.0.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/16"]
  }

  tags = {
    Name = "allow_ssh"
  }
}
