#instance

resource "aws_instance" "aaelogne_ec2_instance_djamel" {
  ami                         = var.ami_id
 # count                       = 1
 # subnet_id                   = aws_subnet.privateSubnets_djamel.id #var.subnet_id
  instance_type               = var.instance_type
  key_name                    = var.ami_key_pair_name
  #security_groups             = [aws_security_group.allow_ssh_djamel.id, aws_security_group.allow_http_djamel.id]
  #associate_public_ip_address = false
  tags                        = { Name = "Djamel" }

network_interface {
     network_interface_id = aws_network_interface.interface_djamel.id
     device_index = 0
  }

  depends_on = [aws_network_interface.interface_djamel,aws_key_pair.generated_key]
}



resource "aws_network_interface" "interface_djamel" {
  subnet_id       = aws_subnet.privateSubnets_djamel.id
  private_ips     = ["192.168.16.12"]
security_groups             = [aws_security_group.allow_ssh_djamel.id, aws_security_group.allow_http_djamel.id]
depends_on=[aws_ec2_client_vpn_network_association.vpn_subnets,aws_ec2_client_vpn_authorization_rule.vpn_auth_rule]
}



