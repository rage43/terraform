#VPC_CLIENT
resource "aws_vpc" "client_pascal_vpc" {              # Creating VPC here
  cidr_block       = var.client_pascal_private_subnet # Defining the CIDR block use 192.168.1.0/30
  instance_tenancy = "default"
  tags             = { Name = "vpc_client_pascal" }
}


#Create private Subnets.
resource "aws_subnet" "privateSubnets_pascal" { # Creating Private Subnets
  vpc_id     = aws_vpc.client_pascal_vpc.id
  cidr_block = var.client_pascal_private_subnet # CIDR block of private subnets
  tags       = { Name = "client_pascal_private_subnets" }
}




#Peering vpc

data "aws_caller_identity" "peer_client_pascal" {
  provider = aws.peer

}


#demande de connection
resource "aws_vpc_peering_connection" "client_pascal" {
  peer_vpc_id = aws_vpc.aaelogne_master_vpc.id
  vpc_id      = aws_vpc.client_pascal_vpc.id

  peer_owner_id = data.aws_caller_identity.peer_client_pascal.account_id
  tags          = { Name = "client_pascal_request" }
  auto_accept   = false

}
# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer_client_pascal" {
  provider                  = aws.peer
  vpc_peering_connection_id = aws_vpc_peering_connection.client_pascal.id
  auto_accept               = true

  tags = {
    Side = "Accepter"
  }
}


#Route table for peering Subnet's
resource "aws_route_table" "client_pascal_route" { # Creating RT for Private Subnet
  vpc_id = aws_vpc.client_pascal_vpc.id
  tags   = { Name = "client_pascal_route" }
  route {
    cidr_block                = "10.0.0.0/16" # Traffic from Public Subnet reaches Internet via Internet Gateway
    vpc_peering_connection_id = aws_vpc_peering_connection.client_pascal.id
  }
}


#Route table Association proxy
resource "aws_route_table_association" "pascal_PrivateRTassociation" {
  subnet_id      = aws_subnet.privateSubnets_pascal.id
  route_table_id = aws_route_table.client_pascal_route.id
}
