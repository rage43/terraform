#https://timeular.com/blog/creating-an-aws-client-vpn-with-terraform/#certificates

variable "root_domain_name" {
  type    = string
  default = "projet.ipssi.net"
}





#creation du certificat
resource "aws_acm_certificate" "vpn_server" {
  private_key       = file("certs/server.key")
  certificate_body  = file("certs/server.crt")
  certificate_chain = file("certs/ca.crt")

}





#charger les clefs 
resource "aws_acm_certificate" "vpn_client_root" {
  private_key       = file("certs/projet.ipssi.net.key")
  certificate_body  = file("certs/projet.ipssi.net.crt")
  certificate_chain = file("certs/ca.crt")

  tags = { Name = "vpn_client_certificate" }
}

#create vpn endpoint

resource "aws_ec2_client_vpn_endpoint" "vpn" {
  description       = "Client VPN example"
  client_cidr_block = "10.0.4.0/22"
  split_tunnel      = true
  vpn_port          = 1194

  vpc_id                 = aws_vpc.aaelogne_master_vpc.id
  security_group_ids     = [aws_security_group.vpn_access.id]
  server_certificate_arn = aws_acm_certificate.vpn_server.arn

  # Client authentication
  authentication_options {
    type                       = "certificate-authentication"
    root_certificate_chain_arn = aws_acm_certificate.vpn_client_root.arn
  }

  connection_log_options {
    enabled = false
  }

  tags = { Name = "vpn_endpoint_projet" }

  depends_on = [
    aws_acm_certificate.vpn_server,
    aws_acm_certificate.vpn_client_root
  ]
}

#vpn subnet

resource "aws_ec2_client_vpn_network_association" "vpn_subnets" {


  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  subnet_id              = aws_subnet.aaelogne_privateSubnets_2.id

  lifecycle {
    // The issue why we are ignoring changes is that on every change
    // terraform screws up most of the vpn assosciations
    // see: https://github.com/hashicorp/terraform-provider-aws/issues/14717
    ignore_changes = [subnet_id]
  }
}


#VPN authorisation rules

resource "aws_ec2_client_vpn_authorization_rule" "vpn_auth_rule" {
  client_vpn_endpoint_id = aws_ec2_client_vpn_endpoint.vpn.id
  target_network_cidr    = aws_subnet.aaelogne_privateSubnets_2.cidr_block
  authorize_all_groups   = true
}
