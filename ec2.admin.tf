#instance

resource "aws_instance" "aaelogne_ec2_instance_admin" {
  ami                         = var.ami_id
#  count                       = 1
  #subnet_id                   = aws_subnet.aaelogne_privateSubnets_2.id #var.subnet_id
  instance_type               = var.instance_type
  key_name                    = var.ami_key_pair_name
  #security_groups             = [aws_security_group.allow_ssh.id]
  #associate_public_ip_address = false
  tags                        = { Name = "Administration" }



network_interface {
     network_interface_id = aws_network_interface.interface_admin.id
     device_index = 0
  }

  depends_on = [aws_network_interface.interface_admin,aws_key_pair.generated_key]
}



resource "aws_network_interface" "interface_admin" {
  subnet_id       = aws_subnet.aaelogne_privateSubnets_2.id
  private_ips     = ["10.0.2.16"]
security_groups             = [aws_security_group.allow_ssh.id]
tags={Name="admin interface"}
depends_on=[aws_ec2_client_vpn_network_association.vpn_subnets,aws_ec2_client_vpn_authorization_rule.vpn_auth_rule]
}
