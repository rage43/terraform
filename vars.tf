variable "master_vpc_cidr" {}
variable "private_subnets_1" {}
variable "private_subnets_2" {}

#All clients subnet
variable "client_pascal_private_subnet" {
  default = "192.168.0.0/28"
}

variable "client_djamel_private_subnet" {
  default = "192.168.16.0/28"
}


# ec2 instance for client

variable "ec2_user" {
  default = "admin"
}

#instance var

variable "instance_name" {}

variable "instance_type" {}

variable "subnet_id" {
  description = "The VPC subnet the instance(s) will be created in"
  default     = "subnet-07ebbe60"
}

variable "ami_id" {}

variable "number_of_instances" {
  description = "number of instances to be created"
  default     = 1
}


variable "ami_key_pair_name" {
  default = "aaelogne_key"
}


