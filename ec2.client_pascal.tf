#instance

resource "aws_instance" "aaelogne_ec2_instance_pascal" {
  ami                         = var.ami_id
#  count                       = 1
 # subnet_id                   = aws_subnet.privateSubnets_pascal.id #var.subnet_id
  instance_type               = var.instance_type
  key_name                    = var.ami_key_pair_name
 # security_groups             = [aws_security_group.allow_ssh_pascal.id, aws_security_group.allow_http_pascal.id]
  #associate_public_ip_address = false
  tags                        = { Name = "Pascal" }


network_interface {
     network_interface_id = aws_network_interface.interface_pascal.id
     device_index = 0
  }

  depends_on = [aws_network_interface.interface_pascal,aws_key_pair.generated_key]
}



resource "aws_network_interface" "interface_pascal" {
  subnet_id       = aws_subnet.privateSubnets_pascal.id
  private_ips     = ["192.168.0.12"]
security_groups             = [aws_security_group.allow_ssh_pascal.id, aws_security_group.allow_http_pascal.id]
tags={Name="Proxy pascal"}
depends_on=[aws_ec2_client_vpn_network_association.vpn_subnets,aws_ec2_client_vpn_authorization_rule.vpn_auth_rule]
}



