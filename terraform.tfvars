
master_vpc_cidr   = "10.0.0.0/16"
private_subnets_1 = "10.0.1.0/24"
private_subnets_2 = "10.0.2.0/24"
instance_name     = "aaelogne-ec2"
ami_id            = "ami-058bd2d568351da34"
instance_type     = "t2.micro"
