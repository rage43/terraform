#Resource to create a SSH private key
resource "tls_private_key" "aaelogne_tlskey" {
  algorithm = "RSA"
  rsa_bits  = 4096
}


resource "aws_key_pair" "generated_key" {



  # Name of key: Write the custom name of your key

  key_name = var.ami_key_pair_name



  # Public Key: The public will be generated using the reference of tls_private_key.terrafrom_generated_private_key

  public_key = tls_private_key.aaelogne_tlskey.public_key_openssh



  # Store private key :  Generate and save private key(aws_keys_pairs.pem) in current directory 

  provisioner "local-exec" {

    command = <<-EOT

      echo '${tls_private_key.aaelogne_tlskey.private_key_pem}' > "${var.ami_key_pair_name}.pem"

      chmod 600 "${var.ami_key_pair_name}.pem"

    EOT

  }

}
