#instance

resource "aws_instance" "aaelogne_ec2_instance_proxy" {
  ami                         = var.ami_id
#  count                       = 1
  subnet_id                   = aws_subnet.aaelogne_privateSubnets_1.id #var.subnet_id
  instance_type               = var.instance_type
  key_name                    = var.ami_key_pair_name
  security_groups             = [aws_security_group.allow_ssh.id,aws_security_group.allow_http.id]
private_ip     = "10.0.1.16"  
associate_public_ip_address = true
  tags                        = { Name = "Proxy-${var.instance_name}" }


  #network_interface {
   #  network_interface_id = aws_network_interface.interface_proxy.id
    # device_index = 0

  #}

  depends_on = [aws_key_pair.generated_key]
}



#resource "aws_network_interface" "interface_proxy" {
 # subnet_id       = aws_subnet.aaelogne_privateSubnets_1.id
  #private_ips     = ["10.0.1.16"]
#security_groups             = [aws_security_group.allow_ssh.id,aws_security_group.allow_http.id]
#tags={Name="Proxy interface"}
#depends_on=[aws_ec2_client_vpn_network_association.vpn_subnets,aws_ec2_client_vpn_authorization_rule.vpn_auth_rule]
#}




