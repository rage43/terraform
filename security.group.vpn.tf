resource "aws_security_group" "vpn_access" {
  vpc_id = aws_vpc.aaelogne_master_vpc.id
  name   = "vpn-admin-sg"

  ingress {
    from_port   = 1194
    protocol    = "UDP"
    to_port     = 1194
    cidr_blocks = ["0.0.0.0/0"]
    description = "Incoming VPN connection"
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = { Name = "VPN" }
}
