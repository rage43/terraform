#VPC_CLIENT
resource "aws_vpc" "client_djamel_vpc" {              # Creating VPC here
  cidr_block       = var.client_djamel_private_subnet # Defining the CIDR block use 192.168.1.0/30
  instance_tenancy = "default"
  tags             = { Name = "vpc_client_djamel" }
}


#Create private Subnets.
resource "aws_subnet" "privateSubnets_djamel" { # Creating Private Subnets
  vpc_id     = aws_vpc.client_djamel_vpc.id
  cidr_block = var.client_djamel_private_subnet # CIDR block of private subnets
  tags       = { Name = "client_djamel_private_subnets" }
}




#Peering vpc

data "aws_caller_identity" "peer_client_djamel" {
  provider = aws.peer

}


#demande de connection
resource "aws_vpc_peering_connection" "client_djamel" {
  peer_vpc_id = aws_vpc.aaelogne_master_vpc.id
  vpc_id      = aws_vpc.client_djamel_vpc.id

  peer_owner_id = data.aws_caller_identity.peer_client_djamel.account_id
  tags          = { Name = "client_djamel_request" }
  auto_accept   = false


}
# Accepter's side of the connection.
resource "aws_vpc_peering_connection_accepter" "peer_client_djamel" {
  provider                  = aws.peer
  vpc_peering_connection_id = aws_vpc_peering_connection.client_djamel.id
  auto_accept               = true

  tags = {
    Side = "Accepter"
  }
}


#Route table for peering Subnet's
resource "aws_route_table" "client_djamel_route" { # Creating RT for Private Subnet
  vpc_id = aws_vpc.client_djamel_vpc.id
  tags   = { Name = "client_djamel_route" }
  route {
    cidr_block                = "10.0.0.0/16" # Traffic from Public Subnet reaches Internet via Internet Gateway
    vpc_peering_connection_id = aws_vpc_peering_connection.client_djamel.id
  }
}


#Route table Association proxy
resource "aws_route_table_association" "djamel_PrivateRTassociation" {
  subnet_id      = aws_subnet.privateSubnets_djamel.id
  route_table_id = aws_route_table.client_djamel_route.id
}
